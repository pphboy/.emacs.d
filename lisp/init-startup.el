
(package-initialize)

;; 编码
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(setq default-buffer-file-conding-system 'utf-8)
;; 垃圾回收阈值，加速启动快
(setq gc-cons-threshold most-positive-fixnum)
;; 字体 
(set-frame-font "JetBrains Mono 13" )
;; 中文字体
(set-fontset-font "fontset-default" 'gb18030' ("微软雅黑" . "unicode-bmp"))
;; 关闭菜单栏
(menu-bar-mode -1)
;; 关闭工具栏
(tool-bar-mode -1)
;; 关闭滚动条
(scroll-bar-mode -1)
;; 关闭启动界面
(setq inhibit-startup-screen t)



(provide 'init-startup)

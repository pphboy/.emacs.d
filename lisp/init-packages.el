;;; package --- Sum packages Configuration

;;; Commentary:
;;; Code:
;; 插件;;
;; 开启树目录
;; (global-set-key [f8] 'neotree-toggle)
;; 开启evil
;; (require 'evil)
;; (evil-mode 1)

(use-package restart-emacs)

(use-package benchmark-init
  :init (benchmark-init/activate)
  :hook (after-init . benchmark-init/deactivate))

;; 删除光标所在当前行pp
(use-package crux
  :bind ("C-c k" . crux-smart-kill-line))

;; 删除连续的非空字符
(use-package hungry-delete
  :bind (("C-c DEL" . hungry-delete-backward))
  :bind (("C-c d" . hungry-delete-forward)))

(use-package drag-stuff
  :bind (("<M-up>" . drag-stuff-up)
	 ("<M-down>" . drag-stuff-down)))

(use-package ivy
  :defer 1
  :demand
  :hook (after-init . ivy-mode)
  :config
  (ivy-mode 1)
  (setq ivy-use-virtual-buffers t
	ivy-initial-inputs-alist nil
	ivy-count-format "%d%d"
	enable-recursive-minibuffers t
	ivy-re-builders-alist '((t . ivy--regex-ignore-order))))

(use-package counsel
  :after (ivy)
  :bind (("M-x" . counsel-M-x)
	 ("C-x C-f" . counsel-find-file)
	 ("C-c f" . counsel-recentf)
	 ("C-c g" . counsel-git)))

(use-package swiper
  :after (ivy)
  :bind (("C-s" . swiper)
	 ("C-r" . swiper-isearch-backward))
  :config (setq swiper-action-recenter t
		swiper-include-line-number-in-search t))
;; 语法检查插件
(use-package flycheck
  :hook (after-init . global-flycheck-mode))
;; 快捷操作
(use-package crux
  :bind (("C-a" . crux-move-beginning-of-line)
	 ("C-c ^" . crux-top-join-line)
	 ("C-x ," . crux-find-user-init-file)
	 ("C-S-d" . crux-duplicate-current-line-or-region)
	 ("C-S-k" . crux-smart-kill-line)))
;; 快捷键查询
(use-package which-key
  :defer nil
  :config (which-key-mode))

;; Mini Buffer


(use-package ivy-posframe
  :defer 1
  :after (ivy)
  :config
  (setq
      ivy-fixed-height-minibuffer nil
      ivy-posframe-parameters
        '((min-width . 90)
	(font .  "JetBrains Mono 13")
	(min-height .,ivy-height)
	(internal-border-width . 3))))

;; 管理窗口
(use-package ace-window
  :bind (("M-o" . ace-window)))

(use-package lsp-mode
  :init
  ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
  (setq lsp-keymap-prefix "C-c l")
  :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
	 (javascript-mode . lsp)
         (vue-mode . lsp)
	 (java-mode . lsp)
         ;; if you want which-key integration
         (lsp-mode . lsp-enable-which-key-integration))
  :commands (lsp lsp-deferred))

;; optionally
(use-package lsp-ui :commands lsp-ui-mode)
;; if you are helm user
(use-package helm-lsp :commands helm-lsp-workspace-symbol)
;; if you are ivy user
(use-package lsp-ivy :commands lsp-ivy-workspace-symbol)
(use-package lsp-treemacs :commands lsp-treemacs-errors-list)

;; optionally if you want to use debugger
(use-package dap-mode)

;;; init-packages.el ends here
(provide 'init-packages)
